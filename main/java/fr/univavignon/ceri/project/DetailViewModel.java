package fr.univavignon.ceri.project;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.univavignon.ceri.project.data.MuseumRepository;
import fr.univavignon.ceri.project.data.database.Item;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private MuseumRepository repository;
    private MutableLiveData<Item> item;

    /*private MutableLiveData<Throwable> webServiceThrowable;
    private MutableLiveData<Boolean> isLoading; // = new MutableLiveData<>();*/

    public DetailViewModel (Application application) {
        super(application);
        repository = MuseumRepository.get(application);
        item = new MutableLiveData<>();
        /*isLoading = repository.getIsLoading();
        webServiceThrowable = repository.getWebServiceThrowable();*/
    }

    public void setItem(String id) {
        repository.getItem(id);
        item = repository.getSelectedItem();
    }

    LiveData<Item> getItem() {
        return item;
    }

    /*LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    void resetWebServiceThrowable() {
        repository.resetWebServiceThrowable();
        webServiceThrowable = repository.getWebServiceThrowable();
    }

    LiveData<Throwable> getWebServiceThrowable() {
        return webServiceThrowable;
    }

    public void loadWeatherCity() {
        repository.loadWeatherCity(city.getValue());
    }*/
}
