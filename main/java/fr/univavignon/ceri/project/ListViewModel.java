package fr.univavignon.ceri.project;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.univavignon.ceri.project.data.MuseumRepository;
import fr.univavignon.ceri.project.data.database.Item;

public class ListViewModel extends AndroidViewModel {
    private MuseumRepository repository;
    private LiveData<List<Item>> collection;

    //private MutableLiveData<Throwable> webServiceThrowable;
    //private MutableLiveData<Boolean> isLoading;

    public ListViewModel (Application application) {
        super(application);
        repository = MuseumRepository.get(application);
        collection = repository.getCollection();
        //isLoading = repository.getIsLoading();
        //webServiceThrowable = repository.getWebServiceThrowable();
    }

    LiveData<List<Item>> getCollection() {
        return collection;
    }

    public void sortCollectionByAlphaOrder() {
        repository.sortCollectionByAlphaOrder();
        collection = repository.getCollection();
    }

    public void sortCollectionByChronoOrder() {
        repository.sortCollectionByChronoOrder();
        collection = repository.getCollection();
    }
/*
    LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    void resetWebServiceThrowable() {
        repository.resetWebServiceThrowable();
        webServiceThrowable = repository.getWebServiceThrowable();
    }

    LiveData<Throwable> getWebServiceThrowable() {
        return webServiceThrowable;
    }


    public void deleteCity(long id) {
        repository.deleteCity(id);
    }*/

    public void loadCollection() {
        repository.loadCollection();
    }


}