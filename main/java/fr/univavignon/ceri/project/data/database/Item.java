package fr.univavignon.ceri.project.data.database;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity(tableName = "item_table") //, indices = {@Index(value = {"name", "description"},//unique = true)})
public class Item {
    @PrimaryKey() //(autoGenerate = true)
    @NonNull
    @ColumnInfo(name="_id")
    private String id;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @NonNull
    @ColumnInfo(name="description")
    private String description;

    @NonNull
    @ColumnInfo(name="categories")
    private String categories;

    @NonNull
    @ColumnInfo(name="timeFrame")
    private String timeFrame;

    @ColumnInfo(name="year")
    private Integer year;

    @ColumnInfo(name="brand")
    private String brand;

    @ColumnInfo(name="technicalDetails")
    private String technicalDetails;

    @ColumnInfo(name="pictures")
    private String pictures;

    @ColumnInfo(name="working")
    private Boolean working;

    public Item(@NonNull String id, @NonNull String name, @NonNull String description, @NonNull String categories, @NonNull String timeFrame, Integer year, String brand, String technicalDetails, String pictures, Boolean working) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.categories = categories;
        this.timeFrame = timeFrame;
        this.year = year;
        this.brand = brand;
        this.technicalDetails = technicalDetails;
        this.pictures = pictures;
        this.working = working;
    }

    public Item(@NonNull String id, @NonNull String name, @NonNull String description, @NonNull List<String> categories, @NonNull List<Integer> timeFrame, Integer year, String brand, List<String> technicalDetails, Map<String,String> pictures, Boolean working) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.setCategories(categories);
        this.setTimeFrame(timeFrame);
        this.year = year;
        this.brand = brand;
        this.setTechnicalDetails(technicalDetails);
        this.setPictures(pictures);
        this.working = working;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    @NonNull
    public String getCategories() {
        return categories;
    }

    public void setCategories(@NonNull String categories) {
        this.categories = categories;
    }

    @NonNull
    public List<String> getListCategories() {
        return Arrays.asList(categories.split(", "));
    }

    public void setCategories(@NonNull List<String> categories) {
        this.categories = TextUtils.join(", ", categories);
    }

    @NonNull
    public String getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(@NonNull String timeFrame) {
        this.timeFrame = timeFrame;
    }

    @NonNull
    public List<Integer> getListTimeFrame() {
        List<String> timeFramesString = new ArrayList<>();
        List<Integer> timeFramesInt = new ArrayList<>();
        timeFramesString = Arrays.asList(timeFrame.split(", "));
        for (String tf : timeFramesString) {
            timeFramesInt.add(Integer.parseInt(tf));
        }
        return timeFramesInt;
    }

    public void setTimeFrame(@NonNull List<Integer> timeFrame) {
        List<String> timeFrames = new ArrayList<>();
        for(int tf : timeFrame) {
            timeFrames.add(String.valueOf(tf));
        }
        this.timeFrame = TextUtils.join(", ", timeFrames);
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        if (year == null) {
            return;
        }
        this.year = year;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        if (brand == null) {
            return;
        }
        this.brand = brand;
    }

    public String getTechnicalDetails() {
        return technicalDetails;
    }

    public void setTechnicalDetails(String technicalDetails) {
        if (technicalDetails == null) {
            return;
        }
        this.technicalDetails = technicalDetails;
    }

    @NonNull
    public List<String> getListTechnicalDetails() {
        return Arrays.asList(technicalDetails.split(", "));
    }

    public void setTechnicalDetails(@NonNull List<String> technicalDetails) {
        if (technicalDetails == null) {
            return;
        }
        this.technicalDetails = TextUtils.join(", ", technicalDetails);
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(String pictures) {
        if (pictures == null) {
            return;
        }
        this.pictures = pictures;
    }

    public HashMap<String, String> getTransformedPictures() {
        HashMap<String, String> picturesMap = new HashMap<>();
        for(String picture: pictures.split("\\|")) {
            if(picture.contains("$"))
                picturesMap.put(picture.split("\\$")[0], picture.split("\\$")[1]);
            else
                picturesMap.put(picture, "");
        }
        return picturesMap;
    }

    public String getFirstPictureId() {
        if (this.pictures!=null) {
            String pic = this.pictures.split("\\|")[0];
            if (pic.contains("$")) {
                return pic.split("\\$")[0];
            }
            else {
                return pic;
            }
        }
        else {
            return null;
        }
    }

    public void setPictures(Map<String, String> pictures) {
        if (pictures == null) {
            return;
        }
        List<String> picturesList = new ArrayList<>();
        for(String key: pictures.keySet())
            picturesList.add(key+"$"+pictures.get(key));
        this.pictures = TextUtils.join("|", picturesList);
    }


    public Boolean getWorking() {
        return working;
    }

    public void setWorking(Boolean working) {
        if (working == null) {
            return;
        }
        this.working = working;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", categories='" + categories + '\'' +
                ", timeFrame='" + timeFrame + '\'' +
                ", year=" + year +
                ", brand='" + brand + '\'' +
                ", technicalDetails='" + technicalDetails + '\'' +
                ", pictures='" + pictures + '\'' +
                ", working=" + working +
                '}';
    }
}
