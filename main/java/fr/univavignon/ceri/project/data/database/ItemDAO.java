package fr.univavignon.ceri.project.data.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ItemDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Item item);

    @Query("SELECT * from item_table")
    LiveData<List<Item>> getCollection();

    @Query("SELECT * from item_table ORDER BY name ASC")
    LiveData<List<Item>> getCollectionByAlphaOrder();

    @Query("SELECT * from item_table ORDER BY year ASC")
    LiveData<List<Item>> getCollectionByChronoOrder();

    @Query("SELECT * FROM item_table WHERE _id = :id")
    Item getItem(String id);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Item item);
}
