package fr.univavignon.ceri.project.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.univavignon.ceri.project.data.database.Item;
import fr.univavignon.ceri.project.data.database.ItemDAO;
import fr.univavignon.ceri.project.data.database.MuseumRoomDatabase;
import fr.univavignon.ceri.project.data.webservice.CMInterface;
import fr.univavignon.ceri.project.data.webservice.MuseumResponse;
import fr.univavignon.ceri.project.data.webservice.MuseumResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.univavignon.ceri.project.data.database.MuseumRoomDatabase.databaseWriteExecutor;

public class MuseumRepository {
    private static final String TAG = MuseumRepository.class.getSimpleName();
    private static volatile MuseumRepository INSTANCE;
    private final CMInterface api;

    private ItemDAO itemDAO;
    private LiveData<List<Item>> collection;
    private MutableLiveData<Item> selectedItem;

    public synchronized static MuseumRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new MuseumRepository(application);
        }

        return INSTANCE;
    }

    /*private MuseumRepository() {
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://api.weather.gov")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(CMInterface.class);
    }*/
    public MuseumRepository(Application application) {
        MuseumRoomDatabase db = MuseumRoomDatabase.getDatabase(application);
        itemDAO = db.itemDao();
        //allCities = cityDao.getAllCities();
        //allCities = (LiveData) cityDao.getSynchrAllCities();
        selectedItem = new MutableLiveData<>();
        collection = itemDAO.getCollection();
        //isLoading = new MutableLiveData<>();
        //webServiceThrowable = new MutableLiveData<>();
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(CMInterface.class);
    }

    public LiveData<List<Item>> getCollection() {
        return collection;
    }

    public MutableLiveData<Item> getSelectedItem() {
        return selectedItem;
    }

    public void sortCollectionByAlphaOrder() {
        collection = itemDAO.getCollectionByAlphaOrder();
    }

    public void sortCollectionByChronoOrder() {
        collection = itemDAO.getCollectionByChronoOrder();
    }

    public void getItem(String id)  {
        Future<Item> fcity = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return itemDAO.getItem(id);
        });
        try {
            selectedItem.setValue(fcity.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /*public void insertItem(Item newItem) {
        databaseWriteExecutor.submit(() -> {
            itemDAO.insert(newItem);
        });
        String res = "";
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != "")
            selectedItem.setValue(newItem);
        return res;
    }*/

    /*public void getItem(String id)  {
        Future<Item> fcity = databaseWriteExecutor.submit(() -> {
            //Log.d(TAG,"selected id="+id);
            return itemDAO.getItem(id);
        });
        try {
            selectedItem.setValue(fcity.get());
            //return fcity.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
            //return null;
        } catch (InterruptedException e) {
            e.printStackTrace();
            //return null;
        }
    }*/

    public void loadCollection() {
        /*api.getCollection().enqueue(
                new Callback<Map<String,MuseumResponse>>() {
                    @Override
                    public void onResponse(Call<Map<String,MuseumResponse>> call,
                                           Response<Map<String,MuseumResponse>> response) {
                        //isLoading.postValue(false);
                        //Log.d("response", response.toString());
                        try {

                                Log.d("response", response.body().toString());
                            /*for (String key: response.body().keySet()) {
                                //Log.d("detail response",key + "=" + response.body().get(key).name);
                            }*
                                for (Map.Entry<String, MuseumResponse> entry : response.body().entrySet()) {
                                    databaseWriteExecutor.submit(() -> {
                                        Log.d("V2 detail response", entry.getKey() + "=" + entry.getValue().toString());
                                    //Item item = new Item(entry.getKey(), entry.getValue().name, entry.getValue().description, entry.getValue().categories, entry.getValue().timeFrame, entry.getValue().year, entry.getValue().brand, entry.getValue().technicalDetails, entry.getValue().pictures, entry.getValue().working);
                                    //Log.d("Test",item.toString());
                                        //Log.d("Verif", "before item");
                                        Item item = itemDAO.getItem(entry.getKey());
                                        Log.d("Verif", "before if");
                                        if (item == null) {
                                            itemDAO.insert(new Item(
                                                    entry.getKey(),
                                                    entry.getValue().name,
                                                    entry.getValue().description,
                                                    entry.getValue().categories,
                                                    entry.getValue().timeFrame,
                                                    entry.getValue().year,
                                                    entry.getValue().brand,
                                                    entry.getValue().technicalDetails,
                                                    entry.getValue().pictures,
                                                    entry.getValue().working));
                                            Log.d("Verif", "insert");
                                        }
                                        else {
                                            //useumResult.transferInfo(entry.getValue(),item);
                                            //itemDAO.update(item);
                                            Log.d("Verif","update");
                                        }
                                        Log.d("Verif", itemDAO.getItem(entry.getKey()).toString());

                                    });
                                    //Log.d("test bug","blalvla");
                                    //MuseumResult.transferInfo(entry.getValue(), item);
                                    //insertItem(item);
                                    //Log.d("Verif", getItem(item.getId()).toString());
                                    //getItem(item.getId());
                                    //Log.d("Verif", itemDAO.getItem(item.getId()).toString());

                                }
                            /*for(Map.Entry<String, MuseumResponse> entry: response.body().entrySet()) {
                                Item item = itemDao.getItem(entry.getKey());
                            }
                            MuseumResult.transferInfo(response.body(), city);
                            updateCity(city);*

                        } catch (Throwable t) {
                            onFailure(call, t); // Lève un execptione lorsque la ville n'existe pas
                        }

                        //Log.d("res", city.toString());
                    }

                    @Override
                    public void onFailure(Call<Map<String,MuseumResponse>> call, Throwable t) {
                        //isLoading.postValue(false);
                        //Log.d("on failure", t.getMessage());
                        //webServiceThrowable.postValue(new Throwable(t.getMessage()));
                    }
                });*/
        api.getCollection().enqueue(
                new Callback<Map<String, MuseumResponse>>() {
                    @Override
                    public void onResponse(Call<Map<String, MuseumResponse>> call, Response<Map<String, MuseumResponse>> response) {
                        for(Map.Entry<String, MuseumResponse> entry: response.body().entrySet()) {
                            //Log.d("Debug", "début");
                            databaseWriteExecutor.submit(() -> {
                                //Log.d("Debug", "Avant item");
                                Item item = itemDAO.getItem(entry.getKey());
                                //Log.d("Debug", "Avant if statement");
                                if(item == null) {
                                    itemDAO.insert(new Item(
                                            entry.getKey(),
                                            entry.getValue().name,
                                            entry.getValue().description,
                                            entry.getValue().categories,
                                            entry.getValue().timeFrame,
                                            entry.getValue().year,
                                            entry.getValue().brand,
                                            entry.getValue().technicalDetails,
                                            entry.getValue().pictures,
                                            entry.getValue().working
                                    ));
                                    //Log.d("Debug", "dans if");
                                }
                                else {
                                    item.setName(entry.getValue().name);
                                    item.setCategories(entry.getValue().categories);
                                    item.setDescription(entry.getValue().description);
                                    item.setTimeFrame(entry.getValue().timeFrame);
                                    item.setYear(entry.getValue().year);
                                    item.setBrand(entry.getValue().brand);
                                    item.setTechnicalDetails(entry.getValue().technicalDetails);
                                    item.setPictures(entry.getValue().pictures);
                                    item.setWorking(entry.getValue().working);
                                    itemDAO.update(item);
                                    //Log.d("Debug", "dans else");
                                }
                                //Log.d("Debug", "après else");
                                Log.d("Debug", itemDAO.getItem(entry.getKey()).toString());
                            });
                            //Log.d("Debug", "fin");

                        }
                    }

                    @Override
                    public void onFailure(Call<Map<String, MuseumResponse>> call, Throwable t) {
                        //webServiceThrowable.postValue(t);
                        Log.d("Erreur", t.getMessage());
                    }
                });

    }
}
