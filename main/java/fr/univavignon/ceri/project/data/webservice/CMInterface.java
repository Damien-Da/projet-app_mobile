package fr.univavignon.ceri.project.data.webservice;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface CMInterface {
    @Headers("Accept: json")
    @GET("collection")
    Call<Map<String,MuseumResponse>> getCollection();
}
