package fr.univavignon.ceri.project.data.webservice;

import android.util.Log;

import fr.univavignon.ceri.project.data.database.Item;

public class MuseumResult {
    public static void transferInfo(MuseumResponse museumResponse, Item item) {
        Log.d("TestDeb",museumResponse.name);
        item.setName(museumResponse.name);
        Log.d("name",item.getName());
        item.setDescription(museumResponse.description);
        Log.d("desc",item.getDescription());
        item.setCategories(museumResponse.categories);
        Log.d("cat",item.getCategories());
        item.setTimeFrame(museumResponse.timeFrame);
        Log.d("timef",item.getTimeFrame());
        item.setYear(museumResponse.year);
        Log.d("year",String.valueOf(item.getYear()));
        item.setBrand(museumResponse.brand);
        Log.d("brand",item.getBrand());
        item.setTechnicalDetails(museumResponse.technicalDetails);
        Log.d("td",item.getTechnicalDetails());
        item.setPictures(museumResponse.pictures);
        Log.d("pic",item.getPictures());
        item.setWorking(museumResponse.working);
        Log.d("work",String.valueOf(item.getWorking()));
        Log.d("TestFin",item.toString());
    }
}
