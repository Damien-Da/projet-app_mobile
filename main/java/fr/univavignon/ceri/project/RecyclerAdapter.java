package fr.univavignon.ceri.project;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import fr.univavignon.ceri.project.data.database.Item;

public class RecyclerAdapter extends RecyclerView.Adapter<fr.univavignon.ceri.project.RecyclerAdapter.ViewHolder> {

    private static final String TAG = fr.univavignon.ceri.project.RecyclerAdapter.class.getSimpleName();

    private List<Item> collection;
    private ListViewModel listViewModel;
    private boolean isCardLeftLayout = true;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v;
        if (isCardLeftLayout) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_left_layout, viewGroup, false);
            isCardLeftLayout = false;
        }
        else {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_right_layout, viewGroup, false);
            isCardLeftLayout = true;
        }
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.itemTitle.setText(collection.get(i).getName());
        viewHolder.itemBrand.setText(collection.get(i).getBrand());
        viewHolder.itemCategories.setText(collection.get(i).getCategories());
        viewHolder.itemTitle.setText(collection.get(i).getName());


        Glide.with(listViewModel.getApplication())
                .load("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+collection.get(i).getId()+"/thumbnail")
                .into(viewHolder.itemIcon);

        /*
        viewHolder.itemDetail.setText(collection.get(i).getCountry());
        if (collection.get(i).getTemperature() == null)
            viewHolder.itemTemp.setText("");
        else
            viewHolder.itemTemp.setText(Math.round(collection.get(i).getTemperature())+" °C");
        if (collection.get(i).getSmallIconUri() == null)
            viewHolder.itemIcon.setImageResource(0);
        else
            viewHolder.itemIcon.setImageDrawable(viewHolder.itemIcon.getResources().getDrawable(viewHolder.itemIcon.getResources().getIdentifier(cityList.get(i).getSmallIconUri(),
                    null, viewHolder.itemIcon.getContext().getPackageName())));
         */
    }

    @Override
    public int getItemCount() {
        //return Book.books.length;
        return collection == null ? 0 : collection.size();
    }

    public void setCollection(List<Item> collection) {
        this.collection = collection;
        notifyDataSetChanged();
    }
    public void setListViewModel(ListViewModel viewModel) {
        listViewModel = viewModel;
    }
    /*
    private void deleteItem(long id) {
        if (listViewModel != null)
            listViewModel.deleteCity(id);
    }
    */

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemTitle;
        TextView itemBrand;
        TextView itemCategories;
        ImageView itemIcon;

        //ActionMode actionMode;
        //long idSelectedLongClick;

        ViewHolder(View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemBrand = itemView.findViewById(R.id.item_brand);
            itemCategories = itemView.findViewById(R.id.item_categories);
            itemIcon = itemView.findViewById(R.id.item_image);

            /*
            ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

                // Called when the action mode is created; startActionMode() was called
                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    // Inflate a menu resource providing context menu items
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.context_menu, menu);
                    return true;
                }

                // Called each time the action mode is shown. Always called after onCreateActionMode, but
                // may be called multiple times if the mode is invalidated.
                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false; // Return false if nothing is done
                }

                // Called when the user selects a contextual menu item
                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_delete:
                            fr.uavignon.ceri.tp3.RecyclerAdapter.this.deleteItem(idSelectedLongClick);
                            Snackbar.make(itemView, "Ville supprimée !",
                                    Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            mode.finish(); // Action picked, so close the CAB
                            return true;
                        case R.id.menu_update:
                            ListFragmentDirections.ActionListFragmentToNewCityFragment action = ListFragmentDirections.actionListFragmentToNewCityFragment();
                            action.setCityNum(idSelectedLongClick);
                            Navigation.findNavController(itemView).navigate(action);
                            return true;
                        default:
                            return false;
                    }
                }

                // Called when the user exits the action mode
                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    actionMode = null;
                }
            };

             */

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    Log.d(TAG,"position="+getAdapterPosition());
                    String id = RecyclerAdapter.this.collection.get(getAdapterPosition()).getId();
                    Log.d(TAG,"id="+id);

                    ListFragmentDirections.ActionListFragmentToDetailFragment action = ListFragmentDirections.actionListFragmentToDetailFragment();
                    action.setItemId(id);
                    Navigation.findNavController(v).navigate(action);



                }
            });


            /*
            itemView.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    idSelectedLongClick = RecyclerAdapter.this.cityList.get((int)getAdapterPosition()).getId();
                    if (actionMode != null) {
                        return false;
                    }
                    Context context = v.getContext();
                    // Start the CAB using the ActionMode.Callback defined above
                    actionMode = ((Activity)context).startActionMode(actionModeCallback);
                    v.setSelected(true);
                    return true;
                }
            });

             */
        }




    }

}