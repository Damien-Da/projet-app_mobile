package fr.univavignon.ceri.project;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.univavignon.ceri.project.data.webservice.MuseumResponse;

public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView nameItem, brandItem, timeFrameItem, categoriesItem, descriptionItem, yearItem, workingItem;
    private ImageView imageItem;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        String itemID = args.getItemId();
        Log.d(TAG,"selected id="+itemID);
        viewModel.setItem(itemID);

        listenerSetup();
        observerSetup();

        /*view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });*/
    }

    private void listenerSetup() {
        nameItem = getView().findViewById(R.id.nameItem);
        brandItem = getView().findViewById(R.id.brand);
        timeFrameItem = getView().findViewById(R.id.editTimeframe);
        categoriesItem = getView().findViewById(R.id.editCategories);
        descriptionItem = getView().findViewById(R.id.editDescription);
        yearItem = getView().findViewById(R.id.editYear);
        workingItem = getView().findViewById(R.id.editWorking);

        imageItem = getView().findViewById(R.id.image);

        //progress = getView().findViewById(R.id.progress);

       /* getView().findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //viewModel.loadWeatherCity();
                /*Snackbar.make(view, "Interrogation à faire du service web",
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();*
            }
        });*/

        getView().findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.univavignon.ceri.project.DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }

    private void observerSetup() {
        viewModel.getItem().observe(getViewLifecycleOwner(),
                item -> {
                    if (item != null) {
                        Log.d(TAG, "observing city view");

                        nameItem.setText(item.getName());
                        brandItem.setText(item.getBrand());
                        timeFrameItem.setText(item.getTimeFrame());
                        categoriesItem.setText(item.getCategories());
                        descriptionItem.setText(item.getDescription());
                        yearItem.setText(String.valueOf(item.getYear()));
                        if (item.getWorking() != null) {
                            if(item.getWorking()) {
                                workingItem.setText("Oui");
                            }
                            else {
                                workingItem.setText("Non");
                            }
                        }

                        if (item.getFirstPictureId() != null) {
                            Glide.with(viewModel.getApplication())
                                    .load("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+item.getId()+"/images/"+item.getFirstPictureId())
                                    .into(imageItem);
                        }


                        /*Map<String,String> pictures = item.getTransformedPictures();
                        Set<String> keys = pictures.keySet();
                        Glide.with(viewModel.getApplication())
                                .load("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+item.getId()+"/images/"+keys)
                                .into(viewHolder.itemIcon);
                        if (city.getTemperature() != null)
                            textTemperature.setText(Math.round(city.getTemperature()) + " °C");
                        if (city.getHumidity() != null)
                            textHumidity.setText(city.getHumidity() + " %");
                        if (city.getCloudiness() != null)
                            textCloudiness.setText(city.getCloudiness() + " %");
                        if (city.getFullWind() != null)
                            textWind.setText((city.getFullWind()));
                        textLastUpdate.setText(city.getStrLastUpdate());

                        // set ImgView
                        if (city.getIconUri() != null)
                            imgWeather.setImageDrawable(getResources().getDrawable(getResources().getIdentifier(city.getIconUri(),
                                    null, getContext().getPackageName())));*/
                        /*Snackbar.make(getView(), "Mis à jour",
                                Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();*/

                    }
                });
    }
}